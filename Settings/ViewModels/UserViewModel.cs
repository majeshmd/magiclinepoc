﻿using MagicLineEntityModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace Settings.ViewModels
{
    public class UserViewModel : ViewModelBase
    {
        public UserViewModel()
        {
            LoadData();
        }
        #region Command
        private ICommand _cancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                return _cancelCommand ?? (_cancelCommand = new CommandHandler(() => ExecCancelCommand(), true));
            }
        }
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand<object>(ExecSaveCommand);
                }
                return _saveCommand;
            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new CommandHandler(() => ExecDeleteCommand(), true));
            }
        }

        public void ExecSaveCommand(object obj)
        {
            var password = obj as PasswordBox;
            if (UserName == null || UserName == "" || password.Password == null || password.Password == "" || FirstName == null || FirstName == "" ||
                LastName == null || LastName == "" || SelectedGroup == null || (IsUserExists() && IsUpdate == false))
            {
                IsValid = false;
            }
            else
            {
                using (var context = new MagicLinePOCEntities())
                {
                    var userNameParameter = new SqlParameter("@UserName", UserName);
                    var passwordParameter = new SqlParameter("@Password", password.Password);
                    var firstNameParameter = new SqlParameter("@FirstName", FirstName);
                    var lastNameParameter = new SqlParameter("@LastName", LastName);
                    var groupIDParameter = new SqlParameter("@GroupID", SelectedGroup.ID);

                    if (IsUpdate)
                    {
                        if (SelectedUser != null)
                        {
                            var IDParameter = new SqlParameter("@ID", SelectedUser.ID);
                            context.Database.ExecuteSqlCommand("exec dbo.UpdateUser @UserName,@Password,@FirstName,@LastName,@GroupID,@ID",
                            userNameParameter, passwordParameter, firstNameParameter, lastNameParameter, groupIDParameter, IDParameter);
                        }
                    }
                    else
                    {
                        context.Database.ExecuteSqlCommand("exec dbo.AddUser @UserName,@Password,@FirstName,@LastName,@GroupID",
                        userNameParameter, passwordParameter, firstNameParameter, lastNameParameter, groupIDParameter);
                    }
                }
                ExecCancelCommand();
                password.Clear();
                LoadData();
            }
        }

        public void ExecDeleteCommand()
        {
            if (selectedUser != null)
            {
                using (var context = new MagicLinePOCEntities())
                {
                    var user = (from item in context.Users
                                where item.ID == selectedUser.ID
                                select item).FirstOrDefault();
                    context.Users.Remove(user);
                    context.SaveChanges();
                }
                ExecCancelCommand();
                LoadData();
            }
        }

        public void ExecCancelCommand()
        {
            FirstName = "";
            LastName = "";
            UserName = "";
            Password = "";
            SelectedGroup = null;
            IsUpdate = false;
        }
        #endregion

        private bool IsUserExists()
        {
            using (var context = new MagicLinePOCEntities())
            {
                var user = new List<User>(context.Users);
                var users = new ObservableCollection<User>(context.Users);
                var count = (from item in users
                             where item.UserName == UserName
                             select item).Count();
                if (count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public void LoadData()
        {
            using (var context = new MagicLinePOCEntities())
            {
                UserGroupCollection = new ObservableCollection<UserGroup>(context.UserGroups);
                UserCollection = new ObservableCollection<User>(context.Users);
            }
        }
        public void LoadSelectedUser()
        {
            if (selectedUser != null)
            {
                FirstName = selectedUser.FirstName;
                LastName = selectedUser.LastName;
                UserName = selectedUser.UserName;
                Password = "";
                IsUpdate = true;
                using (var context = new MagicLinePOCEntities())
                {
                    SelectedGroup = (from item in UserGroupCollection
                                     where item.ID == selectedUser.GroupID
                                     select item).FirstOrDefault();
                }
            }
        }
        private string userName;
        public string UserName
        {
            get { return userName; }
            set { SetProperty(ref userName, value); }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { SetProperty(ref firstName, value); }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { SetProperty(ref lastName, value); }
        }
        private bool isValid = true;

        public bool IsValid
        {
            get { return isValid; }
            set { SetProperty(ref isValid, value); }
        }
        private bool isDropDownOpen;
        public bool IsDropDownOpen
        {
            get { return isDropDownOpen; }
            set { SetProperty(ref isDropDownOpen, value); }
        }

        private bool isUpdate;
        public bool IsUpdate
        {
            get { return isUpdate; }
            set { SetProperty(ref isUpdate, value); }
        }
        private User selectedUser;
        public User SelectedUser
        {
            get { return selectedUser; }
            set
            {
                SetProperty(ref selectedUser, value);
                LoadSelectedUser();
            }
        }
        private UserGroup selectedGroup;
        public UserGroup SelectedGroup
        {
            get { return selectedGroup; }
            set { SetProperty(ref selectedGroup, value); }
        }
        private ObservableCollection<UserGroup> userGroupCollection;
        public ObservableCollection<UserGroup> UserGroupCollection
        {
            get { return userGroupCollection; }
            set { SetProperty(ref userGroupCollection, value); }
        }
        private ObservableCollection<User> useCollection;
        public ObservableCollection<User> UserCollection
        {
            get { return useCollection; }
            set { SetProperty(ref useCollection, value); }
        }

    }
}
