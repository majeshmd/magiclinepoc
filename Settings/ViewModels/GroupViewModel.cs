﻿
using MagicLineEntityModel;
using Settings.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Settings.ViewModels
{
    public class GroupViewModel : ViewModelBase
    {
        public GroupViewModel()
        {
            moduleCollection = new ObservableCollection<ModuleModel>();
            LoadModules();
        }

        #region Command
        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new CommandHandler(() => ExecSaveCommand(), true));
            }
        }
        private ICommand _cancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                return _cancelCommand ?? (_cancelCommand = new CommandHandler(() => ExecCancelCommand(), true));
            }
        }

        public void ExecSaveCommand()
        {
            if (GroupName == null || GroupName == "")
            {
                IsValidGroupName = false;
            }
            else
            {
                IsValidGroupName = true;
                var context = new MagicLinePOCEntities();
                var userGroup = new UserGroup()
                {
                    GroupName = GroupName,
                    IsActive = true

                };
                context.UserGroups.Add(userGroup);
                var id = context.UserGroups.Add(userGroup).ID;

                List<ModuleModel> modules = (from item in moduleCollection
                                             where item.IsSelected == true
                                             select item).ToList();
                foreach (var module in modules)
                {
                    var groupModule = new GroupModule()
                    {
                        GroupID = id,
                        ModuleID = module.ID

                    };
                    context.GroupModules.Add(groupModule);
                }

                context.SaveChanges();
                UserGroupCollection = new ObservableCollection<UserGroup>(context.UserGroups);
                ExecCancelCommand();
            }

        }

        public void ExecCancelCommand()
        {
            GroupName = "";
            moduleCollection.Where(w => w.IsSelected == true).ToList().ForEach(i => i.IsSelected = false);
        }
        #endregion


        public void LoadModules()
        {
            var context = new MagicLinePOCEntities();
            List<Module> modules = context.Modules.ToList();
            foreach (var item in modules)
            {
                var module = new ModuleModel()
                {
                    ID = item.ID,
                    ModuleName = item.ModuleName
                };
                ModuleCollection.Add(module);
            }
            UserGroupCollection = new ObservableCollection<UserGroup>(context.UserGroups);
        }

        private string _groupName;
        public string GroupName
        {
            get { return _groupName; }
            set
            {
                if (value.Trim().Length > 0)
                {
                    IsValidGroupName = true;
                }
                SetProperty(ref _groupName, value);
            }
        }

        private bool isValidGroupName = false;
        public bool IsValidGroupName
        {
            get { return isValidGroupName; }
            set { SetProperty(ref isValidGroupName, value); }
        }
        private ObservableCollection<UserGroup> userGroupCollection;
        public ObservableCollection<UserGroup> UserGroupCollection
        {
            get { return userGroupCollection; }
            set { SetProperty(ref userGroupCollection, value); }
        }

        private ObservableCollection<ModuleModel> moduleCollection;
        public ObservableCollection<ModuleModel> ModuleCollection
        {
            get { return moduleCollection; }
            set { SetProperty(ref moduleCollection, value); }
        }


    }
}
