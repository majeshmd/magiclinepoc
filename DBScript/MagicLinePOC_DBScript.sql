﻿USE [master]
GO
/****** Object:  Database [MagicLinePOC]    Script Date: 17-02-2022 20:47:55 ******/
CREATE DATABASE [MagicLinePOC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MagicLinePOC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MagicLinePOC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MagicLinePOC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MagicLinePOC.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE MagicLinePOC SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MagicLinePOC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MagicLinePOC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MagicLinePOC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MagicLinePOC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MagicLinePOC] SET ARITHABORT OFF 
GO
ALTER DATABASE [MagicLinePOC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MagicLinePOC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MagicLinePOC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MagicLinePOC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MagicLinePOC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MagicLinePOC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MagicLinePOC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MagicLinePOC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MagicLinePOC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MagicLinePOC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MagicLinePOC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MagicLinePOC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MagicLinePOC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MagicLinePOC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MagicLinePOC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MagicLinePOC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MagicLinePOC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MagicLinePOC] SET RECOVERY FULL 
GO
ALTER DATABASE [MagicLinePOC] SET  MULTI_USER 
GO
ALTER DATABASE [MagicLinePOC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MagicLinePOC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MagicLinePOC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MagicLinePOC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MagicLinePOC] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MagicLinePOC] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'MagicLinePOC', N'ON'
GO
ALTER DATABASE [MagicLinePOC] SET QUERY_STORE = OFF
GO
USE [MagicLinePOC]
GO
/****** Object:  Table [dbo].[GroupModule]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupModule](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupID] [bigint] NULL,
	[ModuleID] [bigint] NULL,
 CONSTRAINT [PK_GroupModules] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Module]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Module](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](50) NULL,
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](500) NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[GroupID] [bigint] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[GroupModule] ON 
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (1, 0, 1)
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (2, 0, 2)
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (3, 0, 3)
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (4, 0, 3)
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (5, 0, 1)
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (6, 0, 3)
GO
INSERT [dbo].[GroupModule] ([ID], [GroupID], [ModuleID]) VALUES (7, 0, 6)
GO
SET IDENTITY_INSERT [dbo].[GroupModule] OFF
GO
SET IDENTITY_INSERT [dbo].[Module] ON 
GO
INSERT [dbo].[Module] ([ID], [ModuleName]) VALUES (1, N'Sales')
GO
INSERT [dbo].[Module] ([ID], [ModuleName]) VALUES (2, N'Accounts')
GO
INSERT [dbo].[Module] ([ID], [ModuleName]) VALUES (3, N'Human Resource')
GO
INSERT [dbo].[Module] ([ID], [ModuleName]) VALUES (4, N'All')
GO
INSERT [dbo].[Module] ([ID], [ModuleName]) VALUES (5, N'Support')
GO
INSERT [dbo].[Module] ([ID], [ModuleName]) VALUES (6, N'Report')
GO
SET IDENTITY_INSERT [dbo].[Module] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([ID], [UserName], [Password], [FirstName], [LastName], [GroupID], [IsActive]) VALUES (5, N'Admin', N'煻뙸勼뇵폪ﬢ뵻帛�ᄜ궨妄䗪夦撝鍷댔糲摏ᘩ騌ꭝ䂄曱扗쭃�颖ᐿ', N'Admin', N'Admin', 4, 1)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[UserGroup] ON 
GO
INSERT [dbo].[UserGroup] ([ID], [GroupName], [IsActive]) VALUES (1, N'Sales', 1)
GO
INSERT [dbo].[UserGroup] ([ID], [GroupName], [IsActive]) VALUES (2, N'Accounts', 1)
GO
INSERT [dbo].[UserGroup] ([ID], [GroupName], [IsActive]) VALUES (3, N'HR', 1)
GO
INSERT [dbo].[UserGroup] ([ID], [GroupName], [IsActive]) VALUES (4, N'Admin', 1)
GO
INSERT [dbo].[UserGroup] ([ID], [GroupName], [IsActive]) VALUES (5, N'Support', 1)
GO
SET IDENTITY_INSERT [dbo].[UserGroup] OFF
GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddUser]
(
    @UserName varchar(50),
	@Password NVARCHAR(50),
	@FirstName varchar(50),
	@LastName varchar(50),
	@GroupID bigint
)
AS
BEGIN
    SET NOCOUNT ON

   		DECLARE @salt UNIQUEIDENTIFIER=NEWID()

        INSERT INTO  dbo.[User]( UserName, Password, FirstName, LastName, GroupID, IsActive)
        VALUES( @UserName,HASHBYTES('SHA2_512', @Password),@FirstName,@LastName,@GroupID,1)

END
GO
/****** Object:  StoredProcedure [dbo].[GetUser]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUser]
	(
    @UserName varchar(50),
	@Password NVARCHAR(50)
	)
AS
BEGIN
    SET NOCOUNT ON
		 SELECT * FROM [dbo].[User] WHERE [Password]=HASHBYTES('SHA2_512', @Password) and UserName=@UserName

END
GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 17-02-2022 20:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[UpdateUser]
(
    @UserName varchar(50),
	@Password NVARCHAR(50),
	@FirstName varchar(50),
	@LastName varchar(50),
	@GroupID bigint,
	@ID bigint
)
AS
BEGIN
    SET NOCOUNT ON
	Update dbo.[User]
		Set UserName=@UserName,
			[Password]=HASHBYTES('SHA2_512', @Password),
			FirstName=@FirstName,
			LastName=@LastName,
			GroupID=@GroupID
		Where ID=@ID
END
GO
USE [master]
GO
ALTER DATABASE [MagicLinePOC] SET  READ_WRITE 
GO
