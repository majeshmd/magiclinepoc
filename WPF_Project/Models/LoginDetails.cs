﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_Project.Models
{
    public class LoginDetails
    {
        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set {_UserName = value;}
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private bool _isLoginMe;
        public bool IsLoginMe
        {
            get { return _isLoginMe; }
            set { _isLoginMe = value; }
        }

    }
}
