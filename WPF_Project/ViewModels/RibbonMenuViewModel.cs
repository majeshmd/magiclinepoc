﻿
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using Settings.ViewModels;
using Settings.Views;
using System;
using System.IO;
using System.Windows.Input;
using Unity;
using WPF_Project.Views;

namespace WPF_Project.ViewModels
{
    public class RibbonMenuViewModel: ViewModelBase
    {
        IUnityContainer _unityContainer;
        IRegionManager _regionManager;
        IEventAggregator _eventAggregator;
        public DelegateCommand MessageSent { get; private set; }
        public RibbonMenuViewModel(IEventAggregator eventAggregator, IUnityContainer container, IRegionManager regionManager)
        {
            _eventAggregator = eventAggregator;
            _unityContainer = container;
            _regionManager = regionManager;
            MessageSent = new DelegateCommand(ShowMessage);
            ProjectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
            _eventAggregator.GetEvent<MessageEventAggregator.LoginSuccessEventAggregator>().Subscribe(LoggedIn);
        }
        public RibbonMenuViewModel()
        {
        }
        public string ProjectDirectory { get; set; }
        public string UserDataFilePath { get => $"{ProjectDirectory}\\{"MagicLineMetadata.json"}"; }

        public IRegion Region
        {
            get
            {
                return _regionManager.Regions["MainRegion"];
            }
        }

        public string Message { get; set; }
        void ShowMessage()
        {
            _eventAggregator.GetEvent<MessageEventAggregator.MessageEventAggregator>().Publish("THis message send by EA");
        }

        public void Action()
        {
            ShowMessage();
        }

        private bool _IsValidUser;
        public bool IsValidUser
        {
            get { return _IsValidUser; }
            set { SetProperty(ref _IsValidUser, value); }
        }

        #region Command
        //
        public ICommand LogOutCommand { get { return new DelegateCommand(OnLogOutCommand); } }

        private void OnLogOutCommand()
        {
            IsValidUser = false;
            DeleteFile(UserDataFilePath);
            ActivateView("LogInView");
        }

        public ICommand ClickOnExitCommand { get { return new DelegateCommand(OnExitCommand); } }

        private void OnExitCommand()
        {
            Environment.Exit(0);
        }

        public ICommand ClickCommand { get { return new DelegateCommand(UpdateRenderHeightForMainLayout); } }
        private void UpdateRenderHeightForMainLayout()
        {
            Action();
        }

        public ICommand ClickOnGroupCommand { get { return new DelegateCommand(OnGroupCommand); } }
        private void OnGroupCommand()
        {
            ActivateView(nameof(GroupView));
        }

        private void LoggedIn(string message)
        {
            IsValidUser = true;
            ActivateView("LoggedIn");
        }

        public ICommand ClickOnUserCommand { get { return new DelegateCommand(OnUserCommand); } }
        private void OnUserCommand()
        {
           ActivateView(nameof(UserView));
            
        }

        private void ActivateView(string viewName)
        {
            dynamic view=null;
            switch (viewName)
            {
                case nameof(UserView):
                    view = _unityContainer.Resolve<UserView>();
                    break;
                case nameof(GroupView):
                    view = _unityContainer.Resolve<GroupView>();
                    break;
                case "LogInView":
                    view = _unityContainer.Resolve<LogInView>();
                    break;
                default:
                    view = _unityContainer.Resolve<HomeView>();
                    break;
            }
           
            if (_regionManager.Regions["MainRegion"].GetView(nameof(view)) != null)
            {
                Region.Remove(_regionManager.Regions["MainRegion"].GetView(nameof(view)));
            }
            _regionManager.Regions["MainRegion"].Add(view, nameof(view));
            Region.Activate(view);
        }

        #endregion
        private bool DeleteFile(string filename)
        {
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            return true;
        }
    }
}
