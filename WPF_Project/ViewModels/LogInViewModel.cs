﻿
using Prism.Commands;
using Prism.Events;
using System.Windows.Input;
using MagicLineEntityModel;
using System.Data.SqlClient;
using System.Collections.Generic;
using Settings.ViewModels;
using WPF_Project.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.IO;
using System;

namespace WPF_Project.ViewModels
{
    public class LogInViewModel : ViewModelBase
    {
        IEventAggregator _eventAggregator;
        public DelegateCommand MessageSent { get; private set; }
        public LogInViewModel(IEventAggregator ea)
        {
            _eventAggregator = ea;
            ProjectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
            LoadLoaginDetails();
        }
        private bool _isValidUser;
        public bool IsValidUser
        {
            get { return _isValidUser; }
            set
            {
                SetProperty(ref _isValidUser, value);
            }
        }

        //private bool _isLoginMe;
        //public bool IsLoginMe
        //{
        //    get { return _isLoginMe; }
        //    set
        //    {
        //        SetProperty(ref _isLoginMe, value);
        //    }
        //}

        private LoginDetails _loginDetails = new LoginDetails();
        public LoginDetails LoginDetails
        {
            get { return _loginDetails; }
            set
            {
                SetProperty(ref _loginDetails, value);
            }
        }

        public string ProjectDirectory { get; set; }
        
        public string UserDataFilePath { get => $"{ProjectDirectory}\\{"MagicLineMetadata.json"}"; }

        //$"{author} is the author of {book}.";
            #region Command

        public ICommand PasswordChangedCommand { get { return new DelegateCommand<object>(OnPasswordChanged); } }
        private void OnPasswordChanged(object obj)
        {
            LoginDetails.Password = ((System.Windows.Controls.PasswordBox)obj).Password;
        }

        public ICommand LogInCommand { get { return new DelegateCommand(UpdateRenderHeightForMainLayout); } }
        private void UpdateRenderHeightForMainLayout()
        {

            if (LoginDetails.UserName == null || LoginDetails.UserName == "" || LoginDetails.Password == null || LoginDetails.Password == "")
            {
                IsValidUser = false;
            }
            else
            {
                using (var context = new MagicLinePOCEntities())
                {
                    var userNameParameter = new SqlParameter("@UserName", LoginDetails.UserName.Trim());
                    var passwordParameter = new SqlParameter("@Password", LoginDetails.Password);
                    var user = new List<User>(context.Database.SqlQuery<User>("exec dbo.GetUser @UserName, @Password", userNameParameter, passwordParameter)).Count;
                    if (user > 0)
                    {
                        
                        WriteAllText(UserDataFilePath, SerializeUserDetails());
                        _eventAggregator.GetEvent<MessageEventAggregator.LoginSuccessEventAggregator>().Publish("Login success");
                    }
                    else
                    {
                        IsValidUser = false;
                    }
                }
            }
        }
        #endregion

        private void LoadLoaginDetails()
        {
            var userdetails = ReadAllText(UserDataFilePath);
            DSerializeUserDetails(userdetails);
            if (LoginDetails.IsLoginMe)
            {
                _eventAggregator.GetEvent<MessageEventAggregator.LoginSuccessEventAggregator>().Publish("Login success");
            }
        }
        private string SerializeUserDetails()
        {
            try
            {
                return JsonConvert.SerializeObject(LoginDetails);
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }
        private void DSerializeUserDetails(string data)
        {
            LoginDetails loginDetails = JsonConvert.DeserializeObject<LoginDetails>(data);
            if (loginDetails!= null && loginDetails.IsLoginMe)
            {
                LoginDetails = loginDetails;
            }
        }

        private bool WriteAllText(string filename, string data)
        {
            if (!File.Exists(filename))
            {
                File.Create(filename).Close();
            }
            
            File.WriteAllText(filename, data);
            return true;
        }

        private string ReadAllText(string filename)
        {
            if (File.Exists(filename))
            {
                string readText = File.ReadAllText(filename);
                return readText;
            }
            return "";
        }

    }
    }
