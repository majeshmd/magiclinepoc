﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace WPF_Project
{

    public class RegionManager : IModule
    {
        private readonly IRegionManager _regionManager;
        public RegionManager(IRegionManager regionManager)
        {
            this._regionManager = regionManager;

        }

        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            _regionManager.RegisterViewWithRegion("MainRegion", typeof(Views.LogInView));

        }
    }
}
