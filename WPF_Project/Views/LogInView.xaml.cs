﻿using Prism.Events;
using System.Windows.Controls;
using WPF_Project.ViewModels;

namespace WPF_Project.Views
{
    /// <summary>
    /// Interaction logic for LogInView.xaml
    /// </summary>
    public partial class LogInView : UserControl
    {
        public LogInView()
        {
            InitializeComponent();
        }

        public LogInView(IEventAggregator eventAggregator)
        {
            InitializeComponent();
            DataContext = new LogInViewModel(eventAggregator);
        }
    }
}
